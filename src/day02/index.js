import run from "aocrunner"

const log = (msg, ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n')
}

const computeResult = (you, me) => {
  if (you === me) {
    log('draw', you, me);
    return 'D';
  }
  // Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock
  if ((you === 'R' && me === 'S')
    || (you === 'S' && me === 'P')
    || (you === 'P' && me === 'R')
  ) {
    log('lose', you, me);
    return 'L';
  }

  log('win', you, me);
  return 'W';
};
const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  log('Parsed input', input)

  const map = { A: 'R', B: 'P', C: 'S', X: 'R', Y: 'P', Z: 'S' };
  // shape you selected (1 for Rock, 2 for Paper, and 3 for Scissors)
  const shape = { R: 1, P: 2, S: 3 };
  // outcome of the round (0 if you lost, 3 if the round was a draw, and 6 if you won)
  const outcome = { W: 6, L: 0, D: 3 };
  const throws = input.map((l) => {
    const [y, m] = l.split(' ');
    return [map[y], map[m]];
  });

  const score = throws.reduce((acc, [you, me]) => {
    if (!you || !me) {
      return acc;
    }
    const sVal = shape[me];
    const oVal = outcome[computeResult(you, me)];
    log(you, me, sVal, oVal);
    return acc + sVal + oVal;
  }, 0);

  return score;
}

const computeThrow = (you, outcome) => {
  if (outcome === 'D') return you;

  if (outcome === 'W') {
    if (you === 'S') return 'R';
    if (you === 'P') return 'S';
    if (you === 'R') return 'P';
  }

  // Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock
  if (you === 'R') return 'S';
  if (you === 'S') return 'P';
  if (you === 'P') return 'R';
};
const part2 = (rawInput) => {
  const input = parseInput(rawInput)
  log('Parsed input', input)

  const map = {
    A: 'R',
    B: 'P',
    C: 'S',
    X: 'L',
    Y: 'D',
    Z: 'W',
  };
  const shapes = { R: 1, P: 2, S: 3 };
  const outcomes = { W: 6, L: 0, D: 3 };
  const throws = input.map((l) => {
    const [y, m] = l.split(' ');
    return [map[y], map[m]];
  });

  const score = throws.reduce((acc, [you, outcome]) => {
    const t = computeThrow(you, outcome);
    const sVal = shapes[t];
    const oVal = outcomes[outcome];
    return acc + sVal + oVal;
  }, 0);

  return score;
}

const sampleInput = `
A Y
B X
C Z
`;
run({
  part1: {
    tests: [
      {
        input: sampleInput,
        expected: 15,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: sampleInput,
        expected: 12,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  // onlyTests: true,
})
