import run from "aocrunner"

const debug = (msg='', ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}
const log = (msg='', ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n')
}

const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  // log('Parsed input', input)

  const end = (cycle, x) => {
    debug('e', {x})
    debug(`== Cycle ${cycle} End -- X: ${x[x.length - 1]} ==`)
    debug()
  }

  const x = [1]
  let cmd = null
  let toAdd = null
  let idx = 0
  let totalStrength = 0
  const addStrength = [20, 60, 100, 140, 180, 220]
  for (let cycle=1; cycle<input.length*2; cycle++) {
    const curX = x[x.length - 1]
    const signalStrength = cycle * curX
    debug(`== Cycle ${cycle} Start -- X: ${curX} -- Strength: ${signalStrength} ==`)
    debug('s', {cmd, toAdd, x: curX, cmd: cmd, next: input[idx]})

    if (addStrength.includes(cycle)) {
      log('Interesting Signal Strength', {cycle, x: curX, signalStrength})
      debug('  consider strength', {cycle, signalStrength, totalStrength})
      totalStrength += signalStrength
    }

    if (cmd) {
      debug('  finish:', cmd)
      x.push(curX + toAdd)
      toAdd = null
      cmd = null
      end(cycle, x)
      continue
    }
    if (idx < input.length) {
      const line = input[idx++]
      if (line === 'noop') {
        debug('  begin:', line)
        x.push(curX)
        debug('  finish:', line)
        end(cycle, x)
        continue
      }
      cmd = line
      toAdd = Number(line.split(' ')[1])
      x.push(curX)
      debug('  begin:', cmd)
      end(cycle, x)
    } else if (!cmd) {
      break
    }
  }

  return totalStrength
}

const printCrt = (crt) => {
  crt.forEach(row => {
    console.log(row.join(''))
  })
}

const crt = [
  (new Array(40)).fill(''),
  (new Array(40)).fill(''),
  (new Array(40)).fill(''),
  (new Array(40)).fill(''),
  (new Array(40)).fill(''),
  (new Array(40)).fill(''),
]
const drawPixel = (cycle, x) => {
  debug(`  CRT draws pixel in position ${cycle - 1}`)
  const sprite = [x-1, x, x+1]
  const spritePos = (new Array(40)).fill('.')
  spritePos[x-1] = '#'
  spritePos[x] = '#'
  spritePos[x+1] = '#'
  const col = (cycle -1) % 40
  const row = Math.trunc((cycle -1) / 40)
  debug('  ', {row, col, cycle})
  if (sprite.includes(col)) {
    crt[row][col] = '#'
  } else {
    crt[row][col] = '.'
  }
  // debug(`  sprite pos: \n\t${spritePos.join('')}`)
  // debug(`  cur row: \n\t${crt[0].join('')}`)
  debug(`  cur row: ${crt[0].join('')}`)
}
const part2 = (rawInput) => {
  const input = parseInput(rawInput)
  // log('Parsed input', input)

  let x = 1
  let cmd = null
  let toAdd = null
  let idx = 0

  for (let cycle=1; cycle<input.length*3; cycle++) {
    // debug({cycle, cmd, toAdd, x, next: input[idx]})
    log(`== Cycle ${cycle} Start -- X: ${x} ==`)

    if (cmd) {
      drawPixel(cycle, x)
      x += toAdd
      debug(`  finish executing ${cmd}`)
      log(`== Cycle ${cycle} End   -- X: ${x}\n`)
      toAdd = null
      cmd = null
      continue
    }
    if (idx < input.length) {
      const line = input[idx++]
      if (line === 'noop') {
        debug('  begin executing noop')
        drawPixel(cycle, x)
        debug('  finish executing noop')
        log(`== Cycle ${cycle} End   -- X: ${x}\n`)
        continue
      }
      cmd = line
      toAdd = Number(line.split(' ')[1])
      debug(`  begin executing ${cmd}`)
      drawPixel(cycle, x)
      log(`== Cycle ${cycle} End   -- X: ${x}\n`)
    } else if (!cmd) break
  }
  printCrt(crt)

  return
}

const smallInput = `
noop
addx 3
addx -5`
const sampleInputShort = `addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1`
const sampleInputFull = `addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop`
run({
  part1: {
    tests: [
      // {
      //   input: smallInput,
      //   expected: -1,
      // },
      {
        input: sampleInputFull,
        expected: 13140,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      // {
      //   input: sampleInputShort,
      //   expected: 0,
      // },
      {
        input: sampleInputFull,
        expected: undefined,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  // onlyTests: true,
})
