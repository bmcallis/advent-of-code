import run from "aocrunner"

const log = (msg='', ...rest) => {
  if (rest.length) console.log(msg, ...rest)
  else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n')
}

const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  log('Parsed input', input)

  return
}

const part2 = (rawInput) => {
  log()
  log('*** Part 2 *** *** ***')
  log()

  const input = parseInput(rawInput)
  log('Parsed input', input)

  return
}

const sampleInput = ``
run({
  part1: {
    tests: [
      // {
      //   input: sampleInput,
      //   expected: "",
      // },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      // {
      //   input: sampleInput,
      //   expected: "",
      // },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: true,
})
