import run from "aocrunner"

const log = (msg='', ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n').map(l => l.split(''))
}

const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  log('Parsed input', input)

  let visCnt = (input.length * 2) + ((input[0].length - 2)*2)
  for (let r=1; r<input[0].length-1; r++) {
    for (let c=1; c<input.length-1; c++) {
      const height = input[r][c]
      const left = input[r].slice(0, c)
      const right = input[r].slice(c+1)
      const up = []
      for (let u=0; u<r; u++) { up.push(input[u][c]) }
      const down = []
      for (let d=(r+1); d<input.length; d++) { down.push(input[d][c]) }
      // log({r, c, height, left, right, up, down})
      const leftVis = !left.some(h => h >= height)
      const rightVis = !right.some(h => h >= height)
      const upVis = !up.some(h => h >= height)
      const downVis = !down.some(h => h >= height)
      if (leftVis || rightVis || upVis || downVis) {
        // log('vis', {r, c, height, leftVis, rightVis, upVis, downVis})
        visCnt++
      }
    }
    // log()
  }

  return visCnt
}

const part2 = (rawInput) => {
  log()
  log('*** Part 2 *** *** ***')
  log()

  const input = parseInput(rawInput)
  log('Parsed input', input)

  let allCounts = {}
  let maxScore = 0
  for (let r=0; r<input[0].length-1; r++) {
    for (let c=0; c<input.length-1; c++) {
      const height = input[r][c]
      const left = input[r].slice(0, c)
      const right = input[r].slice(c+1)
      const up = []
      for (let u=0; u<r; u++) { up.push(input[u][c]) }
      const down = []
      for (let d=(r+1); d<input.length; d++) { down.push(input[d][c]) }

      log({r, c, height, left, right})
      const counts = {l:0, r: 0, u:0, d: 0}
      for (let i=(left.length-1); i>=0; i--) {
        counts.l++
        if (left[i] >= height) break
      }
      for (let i=0; i<right.length; i++) {
        counts.r++
        if (right[i] >= height) break
      }
      for (let i=(up.length-1); i>=0; i--) {
        counts.u++
        if (up[i] >= height) break
      }
      for (let i=0; i<down.length; i++) {
        counts.d++
        if (down[i] >= height) break
      }
      allCounts[`${r},${c}`] = counts
      const score = counts.l * counts.r * counts.u * counts.d
      log('  ', counts, score)
      maxScore = Math.max(maxScore, score)
    }
  }
  log()
  log(input)
  log(allCounts)

  return maxScore
}

const sampleInput = `30373
25512
65332
33549
35390`
run({
  part1: {
    tests: [
      {
        input: sampleInput,
        expected: 21,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: sampleInput,
        expected: 8,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  // onlyTests: true,
})
