import run from "aocrunner"

const log = (msg='', ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput
}

const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  // log('Parsed input', input)

  for (let i=4; i < input.length; i++) {
    const set = new Set()
    set.add(input[i-3])
    set.add(input[i-2])
    set.add(input[i-1])
    set.add(input[i])
    if (set.size === 4) {
      return i + 1;
    }
  }

  log('no solution found')
  return
}

const part2 = (rawInput) => {
  log()
  log('*** Part 2 *** *** ***')
  log()

  const input = parseInput(rawInput)
  log('Parsed input', input)

  for (let i=13; i < input.length; i++) {
    const set = new Set()
    set.add(input[i-13])
    set.add(input[i-12])
    set.add(input[i-11])
    set.add(input[i-10])
    set.add(input[i-9])
    set.add(input[i-8])
    set.add(input[i-7])
    set.add(input[i-6])
    set.add(input[i-5])
    set.add(input[i-4])
    set.add(input[i-3])
    set.add(input[i-2])
    set.add(input[i-1])
    set.add(input[i])
    if (set.size === 14) {
      return i + 1;
    }
  }

  log('no solution found')
  return
}

const sampleInput = ``
run({
  part1: {
    tests: [
      // {
      //   input: 'mjqjpqmgbljsphdztnvjfqwrcgsmlb',
      //   expected: 7,
      // },
      // {
      //   input: 'bvwbjplbgvbhsrlpgdmjqwftvncz',
      //   expected: 5,
      // },
      // {
      //   input: 'nppdvjthqldpwncqszvftbrmjlhg',
      //   expected: 6,
      // },
      // {
      //   input: 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg',
      //   expected: 10,
      // },
      // {
      //   input: 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw',
      //   expected: 11,
      // },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: 'mjqjpqmgbljsphdztnvjfqwrcgsmlb',
        expected: 19,
      },
      {
        input: 'bvwbjplbgvbhsrlpgdmjqwftvncz',
        expected: 23,
      },
      {
        input: 'nppdvjthqldpwncqszvftbrmjlhg',
        expected: 23,
      },
      {
        input: 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg',
        expected: 29,
      },
      {
        input: 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw',
        expected: 26,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  // onlyTests: true,
})
