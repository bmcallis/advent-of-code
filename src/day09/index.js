import run from "aocrunner"

const debug = (msg='', ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}
const log = (msg='', ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n').map(l => {
    const [dir, dist] = l.split(' ')
    return [dir, Number(dist)]
  })
}

const replaceAt = (str, idx, val) => str.substring(0, idx) + val + str.substring(idx+1)
const printBoard = ({board, start, rope}) => {
  const out = []
  board.forEach((row, rIdx) => {
    let rowStr = row.join('')
    for (let i=0; i<rope.length; i++) {
      const knot = rope[i]
      if (knot[0] === rIdx) {
        if (rowStr[knot[1]] === '.') {
          const val = (i === 0) ? 'H' : (i === (rope.length - 1)) ? 'T' : i
          rowStr = replaceAt(rowStr, knot[1], val)
        }
      }
    }
    if (start[0] == rIdx && rowStr[start[1]] === '.') {
      rowStr = replaceAt(rowStr, start[1], 's')
    }

    out.push(rowStr)
  })
  return out
}

const isTouching = (a, b) => (Math.abs(a[0] - b[0]) <= 1) && (Math.abs(a[1] - b[1]) <= 1)
const follow = (knotA, knotB) => {
  let newRow = knotB[0]
  if (knotA[0] > knotB[0]) newRow = knotB[0] + 1
  if (knotA[0] < knotB[0]) newRow = knotB[0] - 1
  let newCol = knotB[1]
  if (knotA[1] > knotB[1]) newCol = knotB[1] + 1
  if (knotA[1] < knotB[1]) newCol = knotB[1] - 1
  return [newRow, newCol]
}
const moveRope = (rope, newHead) => {
  rope[0] = newHead
  for (let i=1; i<rope.length; i++) {
    if (!isTouching(rope[i-1], rope[i])) {
      rope[i] = follow(rope[i-1], rope[i])
    }
  }

  return rope
}
const moveHead = (head, dir) => {
  if (dir === 'R') return [head[0], head[1] + 1]
  if (dir === 'L') return [head[0], head[1] - 1]
  if (dir === 'U') return [head[0] - 1, head[1]]
  if (dir === 'D') return [head[0] + 1, head[1]]
}

const part1 = (rawInput) => {
  const numKnots = 2
  const input = parseInput(rawInput)
  log('Parsed input', input)

  const board = new Array(5).fill(new Array(6).fill('.'))
  const start = [board.length - 1, 0]
  let rope = new Array(numKnots).fill(start)
  let head = rope[0]
  const visited = new Set()
  visited.add(start.join(','))

  log('== Initial State ==')
  printBoard({board, start, rope})

  input.forEach(([dir, dist]) => {
    log()
    log(`== ${dir} ${dist} ==`)

    for (let i=0; i<dist; i++) {
      head = moveHead(head, dir)
      rope = moveRope(rope, head)
      const tail = rope[rope.length - 1]
      visited.add(tail.join(','))

      printBoard({board, start, rope})
      log()
    }
  })

  return visited.size
}

const part2 = (rawInput) => {
  log()
  log('*** Part 2 *** *** ***')
  log()

  const numKnots = 10
  const input = parseInput(rawInput)
  log('Parsed input', input)

  const board = new Array(20).fill(new Array(26).fill('.'))
  const start = [15, 11]
  let rope = new Array(numKnots).fill(start)
  let head = rope[0]
  const visited = new Set()
  visited.add(start.join(','))

  log('== Initial State ==')
  log(printBoard({board, start, rope}).join('\n'))

  input.forEach(([dir, dist]) => {
    log()
    debug(`== ${dir} ${dist} ==`)

    for (let i=0; i<dist; i++) {
      head = moveHead(head, dir)
      rope = moveRope(rope, head)
      const tail = rope[rope.length - 1]
      visited.add(tail.join(','))

      log()
    }
    debug('rope', rope)
    debug(printBoard({board, start, rope}).join('\n'))
    // debug()
  })

  printBoard({board, start, rope})
  return visited.size
}

const sampleInput = `R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2`
const sampleInput2 = `R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
`
run({
  part1: {
    tests: [
      {
        input: sampleInput,
        expected: 13,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      // {
      //   input: sampleInput,
      //   expected: 1,
      // },
      {
        input: sampleInput2,
        expected: 36,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  // onlyTests: true
})
