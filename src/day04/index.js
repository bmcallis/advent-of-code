import run from "aocrunner"

const log = (msg, ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n').map(p => p.split(','))
}

const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  log('Parsed input', input)

  let count = 0
  input.forEach(pair => {
    const one = pair[0].split('-').map(Number)
    const two = pair[1].split('-').map(Number)
    log({one, two})
    if (one[0] <= two[0] && one[1] >= two[1]) {
      log('  two in one')
      count++
    }
    else if (two[0] <= one[0] && two[1] >= one[1]) {
      log('  one in two')
      count++
    }
  })

  return count
}

const part2 = (rawInput) => {
  log('')
  log('')
  log('*** Part 2 ***')
  log('')

  const input = parseInput(rawInput)
  log('Parsed input', input)

  let count = 0
  input.forEach(pair => {
    const one = pair[0].split('-').map(Number)
    const two = pair[1].split('-').map(Number)
    log({one, two})
    if (one[0] <= two[0] && two[0] <= one[1]) {
      log('  two0 in one')
      count++
    } else if (one[0] <= two[1] && two[1] <= one[1]) {
      log('  two1 in one')
      count++
    } else if (two[0] <= one[0] && one[0] <= two[1]) {
      log('  one0 in two')
      count++
    } else if (two[0] <= one[1] && one[1] <= two[1]) {
      log('  one1 in one')
      count++
    }
  })

  return count
}

const sampleInput = `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
1-87,1-87
3-7,2-8
2-6,4-8`
run({
  part1: {
    // Within the first pair of Elves, the first Elf was assigned sections 2-4 (sections 2, 3, and 4), while the second Elf was assigned sections 6-8 (sections 6, 7, 8).
    // The Elves in the second pair were each assigned two sections.
    // The Elves in the third pair were each assigned three sections: one got sections 5, 6, and 7, while the other also got 7, plus 8 and 9.
    tests: [
      {
        input: sampleInput,
        expected: 4,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: sampleInput,
        expected: 6,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: true,
})
