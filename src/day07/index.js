import run from "aocrunner"

const log = (msg='', ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const printFilesystem = input => {
  /*
- / (dir)
  - a (dir)
    - e (dir)
      - i (file, size=584)
    - f (file, size=29116)
    - g (file, size=2557)
    - h.lst (file, size=62596)
  - b.txt (file, size=14848514)
  - c.dat (file, size=8504156)
  - d (dir)
    - j (file, size=4060174)
    - d.log (file, size=8033020)
    - d.ext (file, size=5626152)
    - k (file, size=7214296)
  */
}

const parseInput = (rawInput) => {
  return rawInput.split('\n')
}

const computeSize = (tree, path) => {
  // log('computeSize', path)
  // Object.entries(tree).forEach(([path, contents]) => {
  //   let size = contents.reduce((acc, file) => {
  //     if (file.startsWith('dir')) return acc
  //     return acc + Number(file.split(' ')[0])
  //   }, 0)
  //   sizes[path] = {
  //     contents,
  //     size
  //   }
  // })
  const size = tree[path].reduce((acc, file) => {
    if (file.startsWith('dir ')) {
      const nestedPath = `${path}${file.split(' ')[1]}/`
      // log(`  (${path}) again "${file}" '${nestedPath}'`)
      return acc + computeSize(tree, nestedPath)
    }
    const fileSize = Number.parseInt(file.split(' ')[0], 10)
    // log(`  (${path}) ${file.split(' ')[1]}: ${fileSize}`)
    return acc + fileSize
  }, 0)
  // log(path, size)
  if (Number.isNaN(size)) {
    log(path, size)
    process.exit(0)
  }
  return size
}

const part1 = (rawInput) => {
  // return
  const input = parseInput(rawInput)
  log('Parsed input', input)

  const tree = {
    '/': []
  }

  let pwd = ['/']
  for (let i=1; i<input.length; i++) {
    const line = input[i]
    if (line === '$ cd ..') {
      pwd.pop()
      log('moving out', line, pwd.join(''))
      continue
    }
    if (line.startsWith('$ cd')) {
      const dir = line.split('$ cd ')[1]
      pwd.push(dir + '/')
      tree[pwd.join('')] = tree[pwd.join('')] ?? []
      log('moving in', line, pwd.join(''))
      continue
    }
    if (line === '$ ls') {
      log('list', line)
      continue
    }
    tree[pwd.join('')] = [...tree[pwd.join('')], line]
  }
  log(tree)

  const sizes = { }
  let total = 0
  Object.keys(tree).forEach(path => {
    const size = computeSize(tree, path)
    if (size < 100000) {
      total += size
    }
    sizes[path] = size
  })
  log(sizes)

  return total
}

const part2 = (rawInput) => {
  log()
  log('*** Part 2 *** *** ***')
  log()

  const input = parseInput(rawInput)
  log('Parsed input', input)

  const tree = {
    '/': []
  }

  let pwd = ['/']
  for (let i=1; i<input.length; i++) {
    const line = input[i]
    if (!line) {
      continue
    }
    if (line === '$ cd ..') {
      pwd.pop()
      continue
    }
    if (line.startsWith('$ cd')) {
      const dir = line.split('$ cd ')[1]
      pwd.push(dir + '/')
      tree[pwd.join('')] = tree[pwd.join('')] ?? []
      continue
    }
    if (line === '$ ls') {
      continue
    }
    tree[pwd.join('')] = [...tree[pwd.join('')], line]
  }
  log(tree)

  const sizes = [ ]
  let total = 0
  Object.keys(tree).forEach(path => {
    const size = computeSize(tree, path)
    if (Number.isNaN(size)) {
      log(path)
      process.exit(0)
    }
    if (size < 100000) {
      total += size
    }
    sizes.push(size)
  })
  sizes.sort((a, b) => a - b)
  log(sizes)

  const totalSize = 70000000
  const minSize = 30000000
  const unusedSize = totalSize - sizes[sizes.length - 1]
  const need = minSize - unusedSize
  log({unusedSize, need})
  for (let i=0; i<sizes.length; i++) {
    if (sizes[i] > need)
    return sizes[i]
  }

  console.log('\n*** Part 2 ***')
  console.log('no solution found')
  return 0
}

const sampleInput = `$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k`
run({
  part1: {
    tests: [
      {
        input: sampleInput,
        expected: 95437,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: sampleInput,
        expected: 24933642,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: true,
})
