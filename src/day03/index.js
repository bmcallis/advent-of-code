import run from "aocrunner"

const log = (msg, ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n')
}

const priorities = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

const findShared = (a, b) => {
  log({ a, b });
  for (const l of a.split('')) {
    if (b.includes(l)) return l;
  }
}
const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  log('Parsed input', input)

  return input.reduce((acc, line) => {
    const shared = findShared(line.substring(0, line.length / 2), line.substring(line.length / 2));
    return acc + priorities.indexOf(shared) + 1;
  }, 0);
}

const findBadge = (a, b, c) => {
  log({ a, b, c });
  for (const l of a.split('')) {
    if (b.includes(l) && c.includes(l)) return l;
  }
}
const part2 = (rawInput) => {
  const input = parseInput(rawInput)
  log('Parsed input', input)

  let idx = 0;
  let total = 0;
  do {
    const badge = findBadge(input[idx], input[idx + 1], input[idx + 2]);
    total += priorities.indexOf(badge) + 1;
    idx += 3;
  } while (idx < input.length);
  return total;
}

const sampleInput = `vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`;
run({
  part1: {
    tests: [
      {
        input: sampleInput,
        expected: 157,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: sampleInput,
        expected: 70,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  // onlyTests: true,
})
