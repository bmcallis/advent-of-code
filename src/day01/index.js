import run from "aocrunner"

const log = (msg, ...rest) => {
  // if (rest.length) console.log(msg, ...rest)
  // else console.log(msg)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n')
}

const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  log('Parsed input', input)

  let elfCount = 1;
  const max = {elf: 0, calories: 0};
  const byElf = input.reduce((acc, val) => {
    if (val) {
      acc[elfCount] = (acc[elfCount] || 0) + Number(val);
    } else {
      if (acc[elfCount] > max.calories) {
        max.elf = elfCount;
        max.calories = acc[elfCount];
      }
      elfCount++;
    }
    return acc;
  }, {});

  return max.calories;
}

const part2 = (rawInput) => {
  const input = [...parseInput(rawInput), '']
  log('Parsed input', input)

  let elfCount = 0;
  const totals = [];
  const byElf = input.reduce((acc, val) => {
    if (val) {
      acc[elfCount] = (acc[elfCount] || 0) + Number(val);
    } else {
      totals.push(acc[elfCount]);
      elfCount++;
    }
    return acc;
  }, {})
  totals.sort((a, b) => b - a);

  return totals[0] + totals[1] + totals[2];
}

const sampleInput = `
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
`;
run({
  part1: {
    tests: [
      {
        input: sampleInput,
        expected: 24000,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: sampleInput,
        expected: 45000,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  // onlyTests: true,
})
