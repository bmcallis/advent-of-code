import run from "aocrunner"

const log = (msg='', ...rest) => {
  if (rest.length) console.log(msg, ...rest)
  else console.log(msg)
}

const printStacks = (stacks) => {
  const totalStacks = stacks.length;
  const cratesPerStack = stacks.map(s => s.length)
  const maxCrates = Math.max(...cratesPerStack)

  for (let i=maxCrates-1; i>=0; i--) {
    const line = []
    stacks.forEach(s => {
      const crate = s[i]
      if (crate) line.push(`[${crate}] `)
      else line.push('    ')
    })
    log(line.join(''))
  }
  let line = ' '
  for (let i=1; i<=totalStacks; i++) {
    line += `${i}   `
  }
  log(line)
}

const parseInput = (rawInput) => {
  return rawInput.split('\n')
}

const buildStacks = (lines) => {
  const stacks = []
  for (let i=0; i<lines.length; i++) {
    const line = lines[i]
    if (line.includes('[')) {
      for (let j=0; j<line.length; j+=4) {
        if (line[j] == '[') {
          const stack = Math.trunc(j/4)
          if (!stacks[stack]) stacks[stack] = []
          stacks[stack].unshift(line[j+1])
        }
      }
    } else {
      return stacks
    }
  }
  return stacks
}

const moveRe = /move (\d+) from (\d+) to (\d+)/
const part1 = (rawInput) => {
  const input = parseInput(rawInput)
  const stacks = buildStacks(input)
  input.forEach(line => {
    if (line.startsWith('move')) {
      const [_, cnt, from, to] = line.match(moveRe)

      for (let i=0; i<cnt; i++) {
        const crate = stacks[from-1].pop()
        stacks[to-1].push(crate)
      }
    }
  })

  return stacks.reduce((acc, val) => acc + val[val.length - 1], '')
}

const part2 = (rawInput) => {
  const input = parseInput(rawInput)
  const stacks = buildStacks(input)
  input.forEach(line => {
    if (line.startsWith('move')) {
      const [_, cnt, from, to] = line.match(moveRe)
      const fromStack = stacks[from-1]
      stacks[to-1] = [...stacks[to-1], ...fromStack.splice(fromStack.length - cnt, cnt)]
    }
  })

  return stacks.reduce((acc, val) => acc + val[val.length - 1], '')
}

const sampleInput = `    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`
run({
  part1: {
    tests: [
      {
        input: sampleInput,
        expected: "CMZ",
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: sampleInput,
        expected: "MCD",
      },
    ],
    solution: part2,
  },
  trimTestInputs: false,
  // onlyTests: true,
})
